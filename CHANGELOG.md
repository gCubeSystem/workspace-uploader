# Changelog for workspace-uploader

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.1.1] - 2024-07-29

- [#27898] Updated `uploadFile` and `uploadArchive` methods. They include the `fileSize` parameter
- [#27934] Updated the data structure to handle asynchronous uploading of multiple files 

## [v2.1.0] - 2022-05-03

#### Enhancements

- [#23225] Updated the method to read the members of (VRE or Simple) shared folders


## [v2.0.6] - 2020-07-15

- [#19317] Just to include the storagehub-client-wrapper at 1.0.0 version

#### Fixes 

- [#19243] upload archive facility does not work properly with Windows OS

- [#19232] manage file names that use special characters


## [v2.0.5] - 2020-05-14

#### Fixes 

- [#19243] upload archive facility does not work properly with Windows OS

- [#19232] manage file names that use special characters


## [v2.0.4] - 2019-12-19

Updated to Git and Jenkins


## [v2.0.3] - 2019-09-15

[Task #17152] Workspace uploader: avoid . (dot) char added as suffix when the filename does not contain an extension

[Feature #13327] Checking upload permission before the upload starting

[Bug #17450] Workspace: Notifications about file updated (New versions) is never generated


## [v2.0.2] - 2019-04-01

[Incident #16285] Bug fixing

[Feature #13327] Checking permission before uploading


## [v2.0.1] - 2019-01-30

[Bug #13232] Fixing it


## [v2.0.0] - 2018-09-19

[Feature #11722] Workspace Uploader - Update to StorageHUB

In case of 100% added the message "Finalising the upload of [filename]"

[Task #12470] Check uploading of files in the Workpace by the drag and drop facility on Windows Operating System


## [v1.8.0] - 2017-03-27

[Task #7820] DaD and Browse: create new version of existing items added

Removed currUserId parameter from client-side

Updated message displaying also version of file after upload completed


## [v1.7.0] - 2017-02-23

Removed currUserId parameter from client-side

[Feature #6153] Improve error messages

Integrated with file versioning


## [v1.6.0] - 2016-12-01

Feature #5901 Remove ASL Dependency from workspace-uploader Widget

Bug #6096 Fixed


## [v1.4.0] - 2016-05-31

[Feature #4128] Migration to Liferay 6.2


## [v1.3.0] - 2016-05-18

[Feature #4055] DnD multiple instances


## [v1.2.0] - 2016-05-09

[Feature #3962] Sequential upload status


## [v1.1.0] - 2016-01-08

Bug Fixed #1333


## [v1.0.1] - 2015-12-16

Bug Fixed - https://support.d4science.org/issues/1824


## [v1.0.0] - 2015-08-07

first release
