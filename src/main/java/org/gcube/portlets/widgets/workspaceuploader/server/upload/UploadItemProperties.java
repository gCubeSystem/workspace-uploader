package org.gcube.portlets.widgets.workspaceuploader.server.upload;

import java.io.Serializable;

import org.gcube.portlets.widgets.workspaceuploader.client.ConstantsWorkspaceUploader;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Class UploadItemProperties.
 * 
 * contains properties sent from client for any file uploaded
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Aug 1, 2024
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UploadItemProperties implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7226677146789586756L;

	@JsonProperty(ConstantsWorkspaceUploader.CLIENT_UPLOAD_FILEOBJ_FILENAME)
	private String filename;
	@JsonProperty(ConstantsWorkspaceUploader.CLIENT_UPLOAD_FILEOBJ_FILESIZE)
	private Long filesize;

	/**
	 * Instantiates a new upload item properties.
	 */
	public UploadItemProperties() {

	}

	/**
	 * Instantiates a new upload item properties.
	 *
	 * @param filename the filename
	 * @param filesize the filesize
	 */
	public UploadItemProperties(String filename, Long filesize) {
		super();
		this.filename = filename;
		this.filesize = filesize;
	}

	/**
	 * Gets the filename.
	 *
	 * @return the filename
	 */
	@JsonProperty(ConstantsWorkspaceUploader.CLIENT_UPLOAD_FILEOBJ_FILENAME)
	public String getFilename() {
		return filename;
	}

	/**
	 * Gets the filesize.
	 *
	 * @return the filesize
	 */
	@JsonProperty(ConstantsWorkspaceUploader.CLIENT_UPLOAD_FILEOBJ_FILESIZE)
	public Long getFilesize() {
		return filesize;
	}

	/**
	 * Sets the filename.
	 *
	 * @param filename the new filename
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * Sets the filesize.
	 *
	 * @param filesize the new filesize
	 */
	public void setFilesize(Long filesize) {
		this.filesize = filesize;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UploadItemProperties [filename=");
		builder.append(filename);
		builder.append(", filesize=");
		builder.append(filesize);
		builder.append("]");
		return builder.toString();
	}

}
