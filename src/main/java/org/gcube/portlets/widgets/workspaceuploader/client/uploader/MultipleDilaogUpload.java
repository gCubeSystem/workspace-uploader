/**
 *
 */
package org.gcube.portlets.widgets.workspaceuploader.client.uploader;

import java.util.ArrayList;
import java.util.Arrays;

import org.gcube.portlets.widgets.workspaceuploader.client.ConstantsWorkspaceUploader;
import org.gcube.portlets.widgets.workspaceuploader.client.DialogResult;
import org.gcube.portlets.widgets.workspaceuploader.client.uploader.dragdrop.MultipleDNDUpload;
import org.gcube.portlets.widgets.workspaceuploader.shared.HandlerResultMessage;
import org.gcube.portlets.widgets.workspaceuploader.shared.WorkspaceUploadFile;
import org.gcube.portlets.widgets.workspaceuploader.shared.WorkspaceUploaderItem;
import org.gcube.portlets.widgets.workspaceuploader.shared.WorkspaceUploaderItem.UPLOAD_STATUS;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteHandler;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitHandler;
import com.google.gwt.user.client.ui.HTML;

/**
 * The Class MultipleDilaogUploadStream.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Oct 2, 2015
 */
public class MultipleDilaogUpload extends DialogUpload {

	public static final String FILE_DELEMITER = ";";
	private String fileUploadID;
	private MultipleDNDUpload dnd;
	public MultipleDilaogUpload instance = this;
	private String jsonKeys;

	private String idFolder;
	private UPLOAD_TYPE type;

	/**
	 * Instantiates a new multiple dilaog upload stream.
	 *
	 * @param headerTitle the header title
	 * @param parentId    the parent id
	 * @param uploadType  the upload type
	 */
	public MultipleDilaogUpload(String headerTitle, String parentId, UPLOAD_TYPE uploadType) {
		super(headerTitle, parentId, uploadType);

		this.type = uploadType;
		this.idFolder = parentId;

		fileUploadID = GenerateUUID.get(10, 16); // is tagID
		fileUpload.getElement().setId(fileUploadID);
		this.addHandlers();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.user.client.ui.DialogBox#show()
	 */
	@Override
	public void show() {
		super.show();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.user.client.ui.PopupPanel#center()
	 */
	@Override
	public void center() {
		super.center();
	}

	/**
	 * Generate new upload client keys.
	 *
	 * @param files    the files
	 * @param parentId the parent id
	 * @return the list
	 */
	public void generateFakeUploaders(JSONArray filesSelected, String parentId) {

		if (filesSelected == null || filesSelected.size() == 0)
			return;

		// String[] files = filesSelected.split(FILE_DELEMITER);

		// NORMALIZE FILE NAMES
		for (int i = 0; i < filesSelected.size(); i++) {
			JSONObject fileObject = (JSONObject) filesSelected.get(i);
			GWT.log("checking filename for: " + fileObject.getJavaScriptObject().toString());
			String normalizedFileName = fileObject.get("filename").isString().stringValue();
			if (normalizedFileName.contains("\\")) {
				String sanitizedValue = normalizedFileName.substring(normalizedFileName.lastIndexOf("\\") + 1); // remove
																												// C:\fakepath
				// if exists
				JSONValue jsonValue = JSONParser.parseStrict(sanitizedValue);
				fileObject.put("filename", jsonValue);
				filesSelected.set(i, fileObject);
			}
		}

		fakeUploaders = new ArrayList<WorkspaceUploaderItem>(filesSelected.size());
		for (int i = 0; i < filesSelected.size(); i++) {
			JSONObject fileObject = (JSONObject) filesSelected.get(i);
			WorkspaceUploaderItem fakeItem = new WorkspaceUploaderItem();
			fakeItem.setClientUploadKey(GenerateUUID.get());
			fakeItem.setUploadStatus(UPLOAD_STATUS.WAIT);
			WorkspaceUploadFile fakeFile = new WorkspaceUploadFile();
			String fileName = fileObject.get("filename").isString().stringValue();
			Long fileSize = (long) fileObject.get("size").isNumber().doubleValue();
			fakeFile.setFileName(fileName);
			fakeFile.setFileSize(fileSize);
			fakeFile.setParentId(parentId);
			fakeItem.setFile(fakeFile);
			fakeUploaders.add(fakeItem);
		}

		GWT.log("fakeUploaders generated: " + fakeUploaders.toString());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.gcube.portlets.widgets.workspaceuploader.client.uploader.
	 * DialogUploadStream#addHandlers()
	 */
	@Override
	protected void addHandlers() {

		// handle the post
		formPanel.addSubmitCompleteHandler(new SubmitCompleteHandler() {

			@Override
			public void onSubmitComplete(SubmitCompleteEvent event) {
				GWT.log("onSubmitComplete");
				hide();
				// isStatusCompleted = true;
				String result = event.getResults();

				if (result == null) {
					removeLoading();
					new DialogResult(null, "Error during upload", "An error occurred during file upload.").center();
					return;
				}
				String strippedResult = new HTML(result).getText();
				final HandlerResultMessage resultMessage = HandlerResultMessage.parseResult(strippedResult);

				switch (resultMessage.getStatus()) {
				case ERROR:
					removeLoading();
					GWT.log("Error during upload " + resultMessage.getMessage());
					break;
				case UNKNOWN:
					removeLoading();
					GWT.log("Error during upload " + resultMessage.getMessage());
					break;
				case WARN: {
					GWT.log("Upload completed with warnings " + resultMessage.getMessage());
					removeLoading();
					break;
				}
				case SESSION_EXPIRED: {
					GWT.log("Upload aborted due to session expired: " + resultMessage.getMessage());
					Window.alert("Session expired, please reload the page");
					removeLoading();
					break;
				}
				case OK: {
				}

				}
			}

		});

		formPanel.addSubmitHandler(new SubmitHandler() {

			@Override
			public void onSubmit(SubmitEvent event) {
				GWT.log("SubmitEvent");
				addLoading();
				enableButtons(false);
				addNewSubmitToMonitor();
			}
		});

		fileUpload.addChangeHandler(new ChangeHandler() {

			@Override
			public void onChange(ChangeEvent event) {
				GWT.log("browse return...");
				if (fileUpload.getFilename() == null || fileUpload.getFilename().isEmpty()) {
					GWT.log("No file specified ");
					MultipleDilaogUpload.this.hide();
					return;
				}

				GWT.log("Current Uploader has id: " + fileUploadID);
				JavaScriptObject filesSelected = getFilesSelected(fileUploadID);
				
				JSONArray jsonArray = new JSONArray(filesSelected);
				if(jsonArray!=null)
					GWT.log("sono array");
				
				GWT.log("getFilesSelected: " + jsonArray);

				if (isLimitExceeded(jsonArray.size()))
					return;

				// GENERATE NEW UPLOADERS
				generateFakeUploaders(jsonArray, parentIdentifier);
				GWT.log(fakeUploaders.toString());
				createJsonKeyForFiles();
				GWT.log(jsonKeys);

				if (jsonKeys == null) {
					Window.alert("Sorry an error occurred during file/s submit. Try again");
					return;
				}

				// ADD TO FORM PANEL
				// initJsonClientKeys();
				jsonClientKeys.setValue(jsonKeys);
				submitForm();

			}
		});

	}

	/**
	 * Checks if is limit exceeded.
	 *
	 * @param numbOfFiles the numb of files
	 * @return true, if is limit exceeded
	 */
	public boolean isLimitExceeded(int numbOfFiles) {

		if (numbOfFiles > ConstantsWorkspaceUploader.LIMIT_UPLOADS) {
			Window.alert("Multiple upload limit is " + ConstantsWorkspaceUploader.LIMIT_UPLOADS + " files");
			MultipleDilaogUpload.this.hide();
			return true;
		}

		return false;
	}

	/**
	 * Adds the new submit to monitor.
	 */
	public void addNewSubmitToMonitor() {
		GWT.log("addNewSubmitToMonitor...");
		int queueIndex = UploaderMonitor.getInstance().newQueue();
		for (final WorkspaceUploaderItem workspaceUploaderItem : fakeUploaders) {
			UploaderMonitor.getInstance().addNewUploaderToMonitorPanel(workspaceUploaderItem,
					workspaceUploaderItem.getFile().getFileName());
			setVisible(false);
			removeLoading();
			UploaderMonitor.getInstance().addNewUploaderToQueue(queueIndex, workspaceUploaderItem);
//			UploaderMonitor.getInstance().pollWorkspaceUploader(workspaceUploaderItem);
		}

		UploaderMonitor.getInstance().doStartPollingQueue(queueIndex);
	}

	/**
	 * Creates the json key for files.
	 *
	 * @return the string
	 */
	protected void createJsonKeyForFiles() {

		try {
			JSONObject productObj = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			productObj.put(ConstantsWorkspaceUploader.JSON_CLIENT_KEYS, jsonArray);
//			GWT.log("Creating json keys on fakeUploaders: "+fakeUploaders.toString());

			for (int i = 0; i < fakeUploaders.size(); i++) {
				WorkspaceUploaderItem file = fakeUploaders.get(i);
				JSONObject obj = new JSONObject();
				JSONObject fileObject = new JSONObject();
				//Feature #27934
				fileObject.put(ConstantsWorkspaceUploader.CLIENT_UPLOAD_FILEOBJ_FILENAME, new JSONString(file.getFile().getFileName()));
				fileObject.put(ConstantsWorkspaceUploader.CLIENT_UPLOAD_FILEOBJ_FILESIZE, new JSONNumber(file.getFile().getFileSize()));
				obj.put(file.getClientUploadKey(), fileObject);
				jsonArray.set(i, obj);
			}

			jsonKeys = productObj.toString();
			GWT.log("updated jsonKeys: " + jsonKeys);
		} catch (Exception e) {
			GWT.log("error " + e.getMessage());
			jsonKeys = null;
		}
	}

	/**
	 * Submit form.
	 */
	@Override
	public void submitForm() {
		formPanel.submit();
	}

	/**
	 * Gets the files selected.
	 *
	 * @param iFrameName the i frame name
	 * @return the files selected
	 */
	private static native String stopIFrame(final String iFrameName) /*-{
		console.log("iFrameName: " + iFrameName);
		//	   	var iframe= window.frames[iFrameName];
		var iframe = $wnd.$('iframe[name=' + iFrameName + ']', parent.document)[0];
		var iframewindow = iframe.contentWindow ? iframe.contentWindow
				: iframe.contentDocument.defaultView;
		if (iframe == null)
			console.log("iframe is null");
		else
			console.log("iframe is not null");

		if (navigator.appName == 'Microsoft Internet Explorer'
				&& iframewindow.document.execCommand) { // IE browsers
			console.log("IE browsers");
			iframewindow.document.execCommand("Stop");
		} else { // other browsers
			console.log("other browsers");
			iframewindow.stop();
		}

	}-*/;

	/**
	 * Gets the parent id.
	 *
	 * @return the parentId
	 */
	public String getParentId() {
		return parentIdentifier;
	}

	/**
	 * Gets the upload type.
	 *
	 * @return the uploadType
	 */
	public UPLOAD_TYPE getUploadType() {
		return uploadType;
	}

	/**
	 * Gets the files selected.
	 *
	 * @param tagId         the tag id
	 * @param fileDelimiter the file delimiter
	 * @return the files selected
	 */
	public static native JavaScriptObject getFilesSelected(final String tagId) /*-{
		var count = $wnd.$("#" + tagId)[0].files.length;
		console.log("number of files: "+count);
		var outputs = [];

		for (i = 0; i < count; i++) {
			var file = $wnd.$("#" + tagId)[0].files[i];
			//out += file.name + fileDelimiter + file.size + fileDelimiter;
			var the_file = { "filename": file.name, "size": file.size };
			//var jsonString = JSON.stringify(the_file);
			//console.log("the file is: "+jsonString);
			outputs.push(the_file);
		}
		console.log("returning outputs: "+outputs);
		return outputs;
	}-*/;
}
